import QtQuick 2.0

/**
 * This is quick and dirty model for the keys of the InputPanel *
 * The code has been derived from
 * http://tolszak-dev.blogspot.de/2013/04/qplatforminputcontext-and-virtual.html
 * Copyright 2015 Uwe Kindler
 * Licensed under MIT see LICENSE.MIT in project root
 */
Item {
    property QtObject zerothRowModel: zeroth
    property QtObject firstRowModel: first
    property QtObject secondRowModel: second
    property QtObject thirdRowModel: third

    ListModel {
        id:zeroth
        ListElement { letter: "1"; firstSymbol: "1"; keycode: Qt.Key_1 }
        ListElement { letter: "2"; firstSymbol: "2"; keycode: Qt.Key_2 }
        ListElement { letter: "3"; firstSymbol: "3"; keycode: Qt.Key_3 }
        ListElement { letter: "4"; firstSymbol: "4"; keycode: Qt.Key_4 }
        ListElement { letter: "5"; firstSymbol: "5"; keycode: Qt.Key_5 }
        ListElement { letter: "6"; firstSymbol: "6"; keycode: Qt.Key_6 }
        ListElement { letter: "7"; firstSymbol: "7"; keycode: Qt.Key_7 }
        ListElement { letter: "8"; firstSymbol: "8"; keycode: Qt.Key_8 }
        ListElement { letter: "9"; firstSymbol: "9"; keycode: Qt.Key_9 }
        ListElement { letter: "0"; firstSymbol: "0"; keycode: Qt.Key_0 }
    }
    ListModel {
        id:first
        ListElement { letter: "q"; firstSymbol: "!"; keycode: Qt.Key_Q}
        ListElement { letter: "w"; firstSymbol: "@"; keycode: Qt.Key_W}
        ListElement { letter: "e"; firstSymbol: "#"; keycode: Qt.Key_E}
        ListElement { letter: "r"; firstSymbol: "$"; keycode: Qt.Key_R}
        ListElement { letter: "t"; firstSymbol: "%"; keycode: Qt.Key_T}
        ListElement { letter: "y"; firstSymbol: "^"; keycode: Qt.Key_Y}
        ListElement { letter: "u"; firstSymbol: "&"; keycode: Qt.Key_U}
        ListElement { letter: "i"; firstSymbol: "*"; keycode: Qt.Key_I}
        ListElement { letter: "o"; firstSymbol: "("; keycode: Qt.Key_O}
        ListElement { letter: "p"; firstSymbol: ")"; keycode: Qt.Key_E}
    }
    ListModel {
        id:second
        ListElement { letter: "a"; firstSymbol: "~"}
        ListElement { letter: "s"; firstSymbol: "`"}
        ListElement { letter: "d"; firstSymbol: "_"}
        ListElement { letter: "f"; firstSymbol: "-"}
        ListElement { letter: "g"; firstSymbol: "+"}
        ListElement { letter: "h"; firstSymbol: "="}
        ListElement { letter: "j"; firstSymbol: "{"}
        ListElement { letter: "k"; firstSymbol: "}"}
        ListElement { letter: "l"; firstSymbol: "\\"}
    }
    ListModel {
        id:third
        ListElement { letter: "z"; firstSymbol: "["}
        ListElement { letter: "x"; firstSymbol: "]"}
        ListElement { letter: "c"; firstSymbol: "|"}
        ListElement { letter: "v"; firstSymbol: ":"}
        ListElement { letter: "b"; firstSymbol: ";"}
        ListElement { letter: "n"; firstSymbol: "\""}
        ListElement { letter: "m"; firstSymbol: "'"}
    }
}
