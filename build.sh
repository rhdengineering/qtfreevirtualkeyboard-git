#!/bin/bash

set -e
echo FREE-VIRTUAL-KEYBOARD

if [ "x$ARCH" == "xarm" ]; then
	QT_VERS=~/QT_toolchains/host-tools
	_qmake="-spec $QT_VERS/mkspecs/devices/linux-rite-hite-g++"
else
	ARCH=x86
	QT_VERS="/opt/Qt5.6.0/5.6/gcc_64"
	_qmake="-spec linux-g++"
fi

pushd src >/dev/null 2>/dev/null

$QT_VERS/bin/qmake virtualkeyboard.pro -r $_qmake

make clean
make -j8
file libfreevirtualkeyboardplugin.so

# Move out of project directory
popd >/dev/null 2>/dev/null

# Copy QtFreeVirtualKeyboard files into source project's location
QT_SRC=/opt/Qt5.6.0/5.6/gcc_64
#sudo mkdir -v $QT_SRC/qml/QtQuick/FreeVirtualKeyboard
sudo cp -v src/FontAwesome.otf $QT_SRC/qml/QtQuick/FreeVirtualKeyboard/FontAwesome.otf
sudo cp -v src/InputPanel.qml $QT_SRC/qml/QtQuick/FreeVirtualKeyboard/InputPanel.qml
sudo cp -v src/KeyButton.qml $QT_SRC/qml/QtQuick/FreeVirtualKeyboard/KeyButton.qml
sudo cp -v src/KeyModel.qml $QT_SRC/qml/QtQuick/FreeVirtualKeyboard/KeyModel.qml
sudo cp -v src/KeyPopup.qml $QT_SRC/qml/QtQuick/FreeVirtualKeyboard/KeyPopup.qml
sudo cp -v src/qmldir $QT_SRC/qml/QtQuick/FreeVirtualKeyboard/qmldir
sudo cp -v src/libfreevirtualkeyboardplugin.so $QT_SRC/plugins/platforminputcontexts/libfreevirtualkeyboardplugin.so

# Copy QtFreeVirtualKeyboard files into QT_toolchains location
QT_DIR=/home/beta/QT_toolchains/target/qt5
#sudo mkdir -v $QT_DIR/qml/QtQuick/FreeVirtualKeyboard
sudo cp -v src/FontAwesome.otf $QT_DIR/qml/QtQuick/FreeVirtualKeyboard/FontAwesome.otf
sudo cp -v src/InputPanel.qml $QT_DIR/qml/QtQuick/FreeVirtualKeyboard/InputPanel.qml
sudo cp -v src/KeyButton.qml $QT_DIR/qml/QtQuick/FreeVirtualKeyboard/KeyButton.qml
sudo cp -v src/KeyModel.qml $QT_DIR/qml/QtQuick/FreeVirtualKeyboard/KeyModel.qml
sudo cp -v src/KeyPopup.qml $QT_DIR/qml/QtQuick/FreeVirtualKeyboard/KeyPopup.qml
sudo cp -v src/qmldir $QT_DIR/qml/QtQuick/FreeVirtualKeyboard/qmldir
sudo cp -v src/libfreevirtualkeyboardplugin.so $QT_DIR/plugins/platforminputcontexts/libfreevirtualkeyboardplugin.so
